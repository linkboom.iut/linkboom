Bienvenue surce gitlab destiné à expliquer notre démarche et notre projet.
Nom projet :
LinkBoom

Encadrant :
Julien Arsouze

Structure demandeuse :
activité d’accompagnement et de développement de projets artistiques.

Contexte :
Les besoins en communication nécessitent une présence sur un certain nombre de plateformes différentes en fonction des publics ciblés (fans, professionnels, partenaires de
projets, etc...).
Chaque plate-forme demande un mode de communication propre mais les besoins de
gain de temps et de productivité amènent à chercher à développer des process et de
soutils tout-en-un à partir d'éléments de communication similaires voire identiques (format
de post image, vidéo, texte, ...)
Ex : un label avec trois artistes différents publiant des disques, faisant des concerts, et
communiquant avec des fans, des tourneurs, des organisateurs de concerts, dans
différentes régions, et dont la musique est trouvable chez des disquaires, des plateformes
en lignes avec parfois des clips, des photos ou articles de presse à partager/relayer. La
communication passant par les pages indépendantes des artistes et/ou en plus par celles
du label lui-même dont les followers ne sont pas les mêmes et appartiennent à des publics
cibles différents à quion ne s'adresse pas de la même manière

Besoins & objectifs désirés :
Un outil permettant de :
• gérer ses différents comptes sur les réseaux sociaux.
• centraliser, coordonner, programmer les publications
• gérer les publications en fonction des différents contacts et publics cible (liste de
réseaux de publication mobulable ajout/suppression des destinations)
• gérer les publications en fonction de chaque artiste accompagné ou en tant que
structure globale
• gérer ses listes de hashtags pour gagner du temps à chaque publication
• proposer un prétraitement des images, videos, … à base de templates adaptés aux
réseaux utilisés (post, story, bannière,...).
• prévisualisation en fonction des formats et longueurs de textes, hashtags, taille
d’images et de vidéos, ...
Pour résumer, l’outil proposé correspondra à un Hootsuite/Later/etc... amélioré avec des
fonctions (actualisées/actualisables) de prévisualisation pour un cross-posting fluide
(photo/video/texte/metadonnées/hashtags) et moins chronophages à destination d'une
palette de réseaux sociaux (liste non exhaustive Instagram, FB, Youtube, vimeo, LinkedIn,
Twitter…)
Enfin, cet outil devra prendre de préférence la forme d’une application web responsive.
L'option Android et iOS pour smartphone serait bien entendu un plus. L'outil pourra
néanmoins éventuellement être une application desktop mais devant être utilisable sur
toutes les plateformes (Windows, Linux, Mac OS)

/!\ous devons vous signaler le fait que nous n'avons pas encore de site web à vous montrer nous sommes en phase de documentation (MCD, diagrammes de classes...)/!\
Cependant je vous laisse le lien de notre maquette (veillez a bien rester sur le flow 1, vous pouvez vous balader dans la maquette comme bon vous semble):
https://www.figma.com/proto/LuQ0hdyE4Fxqws2rLvf9xq/Untitled?node-id=2%3A7&scaling=contain&page-id=0%3A1&starting-point-node-id=2%3A7&show-proto-sidebar=1
